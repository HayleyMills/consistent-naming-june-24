### Purpose

This workflow will change the names of the files in the input directory.

### Input dir

- pdfs.txt
    - rows of current new name and current name
    - for example: 
      ```
      us_covid_q4	input/W4-covid-19-questionnaire.pdf          
      ```

- pdf files that need their name changed

### Output artifacts

- pdf files with name changed
